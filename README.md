# FairCoin Exporter

[![Build status](https://travis-ci.org/mmoya/faircoin_exporter.svg)][travis]

Prometheus exporter for FairCoin CVN metrics.

This is an exporter for monitoring the health of [CVN][1] in the FairCoin
network. A dashboard can be seen [here][2].


[1]: https://github.com/faircoin/faircoin/blob/master/doc/CVN-operators-guide.md
[2]: https://dashboard.faircoin.io/dashboard/db/faircoin-cvn
